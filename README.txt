
INTRODUCTION
------------
Linkit Node Advanced is an alternative plugin to linkit node for the linkit module.
It allows for some additional configurable parameters.

INSTALLATION
------------

Install as usual.

CONFIGURATION
-------------

Head over to your module settings page, and find the Linkit Node Advanced Page.
The defaults will function exactly like the linkit node module, flip on the extra
info you'd like to see/search by, and set it and forget it.

NOTE
----

As of Drupal 6.x there's not a way to include conflict information for modules.
For best results use this module INSTEAD of the linkit node module, otherwise you'll
end up with some confusing results :D

TODO
----

-Add proper cleanup on uninstall (just need to delete two system vars)
-look into moving this to integrate with views, leaning towards no to keep it light.